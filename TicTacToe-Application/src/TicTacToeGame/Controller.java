package TicTacToeGame;


import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Igor
 */
public class Controller {
    public BoardPosition GetNextMove(Model model, BoardValue moveForValue)
    {
        List<BoardPosition> allEmptyPositions = model.positionsWithValue(BoardValue.Empty);

        // all moved made - fin
        if (allEmptyPositions.isEmpty())
            return null;
        
        // all empty - start in the center
        if (allEmptyPositions.size() == 9)
            return BoardPosition.Center;
        
        /*
         * todo: iz 25 jan 2015
         * Implement smart logic
         */
        
        return allEmptyPositions.get(0);
    }
    
    public String toString()
    {
        return "TicTacToe Controller";
    }
}
