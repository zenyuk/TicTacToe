package TicTacToeGame;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Igor
 */
public class Model {
    public BoardValue LeftTop;
    public BoardValue Top;
    public BoardValue RightTop;
    public BoardValue Left;
    public BoardValue Center;
    public BoardValue Right;
    public BoardValue LeftBottom;
    public BoardValue Bottom;
    public BoardValue RightBottom;
    
    public List<BoardValue> allValues()
    {
        List<BoardValue> result = new ArrayList<>();
        result.add(LeftTop);
        result.add(Top);
        result.add(RightTop);
        result.add(Left);
        result.add(Center);
        result.add(Right);
        result.add(LeftBottom);
        result.add(Bottom);
        result.add(RightBottom);
        
        return result;
    }
    
    public List<BoardPosition> positionsWithValue(BoardValue value)
    {
        List<BoardPosition> result = new ArrayList<>();
        
        if (LeftTop == value)
            result.add(BoardPosition.LeftTop);
        
        if (Top == value)
            result.add(BoardPosition.Top);
        
        if (RightTop == value)
            result.add(BoardPosition.RightTop);
        
        if (Left == value)
            result.add(BoardPosition.Left);
        
        if (Center == value)
            result.add(BoardPosition.Center);
        
        if (Right == value)
            result.add(BoardPosition.Right);
        
        if (LeftBottom == value)
            result.add(BoardPosition.LeftBottom);
        
        if (Bottom == value)
            result.add(BoardPosition.Bottom);
        
        if (RightBottom == value)
            result.add(BoardPosition.RightBottom);
    
        return result;
    }
    
    public boolean isValid()
    {
        // max possible number of crosses or circles on the board
        List<BoardValue> boardState = this.allValues();
        
        Stream<BoardValue> allCrosses = boardState
                .stream()
                .filter(v -> v == BoardValue.Cross);
        
        Stream<BoardValue> allCircles = boardState
                .stream()
                .filter(v -> v == BoardValue.Circle);
        
        return (allCrosses.count() < 6) && (allCircles.count() < 6);
    }
}
